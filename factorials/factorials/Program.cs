﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace factorials
{
    internal class Program
    {
        public static int prompt;
        public static int initial = 1;
        public static bool isValid = false;
        public static bool goodRange = false;
        static void Main()
        {
            //for loop method
            Prompter();
            FactoCalculate(ref prompt, ref initial);
            WriteLine("Answer is: {0}",initial);

            //recursive method
            isValid = false;
            goodRange = false;
            Prompter();
            int recusriveAnswer = FactoCalculateRecursive(prompt);
            WriteLine("Answer is: {0}",recusriveAnswer);

            //'fancy' ending to practive ConsoleKeyInfo system inner struct
            ConsoleKeyInfo keyInfo = Console.ReadKey();
            while (keyInfo.Key != ConsoleKey.Enter)
            {
                keyInfo = Console.ReadKey();
                if (keyInfo.Key == ConsoleKey.Enter) Environment.Exit(0);
            }
        }

        private static void FactoCalculate(ref int x, ref int initial)
        {
            //factorial algorithm with conventional for loop
            for (int i = x; i > 0; i--)
            {
                initial = initial * i;
            }
        }

        private static int FactoCalculateRecursive(int z)
        {
            //factorial algorithm with recursive function
            if (z == 1)
            {
                return 1;
            }
            else
            {
                z = z * FactoCalculateRecursive(z - 1);
                return z;
            }
        }

        public static void Prompter()
        {

            while (!isValid || !goodRange)
            {
                WriteLine("Enter a number below 12 to get its factorial: ");
                string y = ReadLine();
                isValid = int.TryParse(y, out prompt);
                goodRange = prompt <= 12;
            }
        }
    }
}
